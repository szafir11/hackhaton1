<?php 

class  Hackathon{
	
	private $_data = array(
		'newComments' => 0,
		'newReply' => 0,
		'newProofs' => 0,
		'processingProofs' => 0,
		'fileToProcess' => array(),
		'messages' => array()
	);
	private $_file = 'data.txt';
	
	public function __construct() {
		if(file_exists($this->_file)) {
			$data = file_get_contents($this->_file);
			$this->_data = (array)json_decode($data, true);
		}
		else {
			file_put_contents($this->_file, json_encode($this->_data));
		}
		
		$this->checkProcessesFiles();
	}
	
	public function update() {
		file_put_contents($this->_file, json_encode($this->_data));
	}
	
	public function __toString() {
		$data = $this->_data;
		return json_encode($data);
	}
	
	public function addFileToProcess($link) {
		$this->_data['processingProofs']++;
		$this->_data['fileToProcess'][] = array(
			'file' => $link,
			'processTime' => time() + rand(10, 30)
		);
	}
	
	private function checkProcessesFiles() {
		$files = (array)$this->_data['fileToProcess'];
		foreach($files as $key => $file) {
			$file = (array)$file;
			if($file['processTime'] < time()) {
				$this->_data['newProofs']++;
				$this->_data['messages'][] = array(
					"message" =>  "File has been processed.<br><a href='{$file['file']}'>Open proof details page.</a>",
					"type" => 'proofs'
				);
				unset($this->_data['fileToProcess'][$key]);
				//if(isset($this->_data['fileToProcess'][$key])) {
					 //unset($this->_data['fileToProcess'][$key]);
				//}
				//unset($this->_data['fileToProcess']->$key);
			}
		}
	}
	
	public function clearStats() {
		$this->_data['messages'] = array();
		$this->_data['newComments'] = 0;
		$this->_data['newProofs'] = 0;
		$this->_data['newReply'] = 0;
		$this->_data['processingProofs'] = 0;
	}
	
	public function addComment() {
		$this->_data['newComments']++;
		$this->_data['messages'][] = array(
			"type" => "comments",
			'message' => '<a href="" class="message-link">You have new comment.</a>'
		);
	}
	
	public function addReply() {
		$this->_data['newReply']++;
		$this->_data['messages'][] = array(
			"type" => "replies",
			'message' => '<a href="" class="message-link">You have new reply.</a>'
		);
	}
}


