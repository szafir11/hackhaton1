console.log("RUN");
var notificationId = 0;
var server = "http://siteslab.pl/hackathon/ep.php";
var startupRoutine = function(){
	chrome.alarms.create("pollAlarm", {
		periodInMinutes: 0.1
	});
	fetchNotifications();
};

var updateNotifications = function(){
	if(!localStorage.notifications){
		clearNotifications();
	}
	msg = JSON.parse(localStorage.notifications);
	if(localStorage.loggedIn === "true" && msg.newTotal>0){
		chrome.browserAction.setBadgeText({text: ''+msg.newTotal});
	}else{
		chrome.browserAction.setBadgeText({text: ''});
	}
	var finalMsg = {
		type: "notificationsUpdated",
		data: msg
	};
	chrome.runtime.sendMessage(null, finalMsg);
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
	    chrome.tabs.sendMessage(tabs[0].id, finalMsg, function() {});
	});
};

var clearNotifications = function(){
	localStorage.notifications = JSON.stringify({
		"newComments":0,
		"newReply":0,
		"newProofs":0,
		"newTotal":0,
		"processingProofs":0,
	});
	updateNotifications();
}

var fetchNotifications = function(){
	$.get(server+"?notifications", function(data){
		if(localStorage.loggedIn !== "true"){
			return;
		}
		var data = JSON.parse(data);
		if(!data){
			return;
		}
		data.newTotal = 0;
		if(!localStorage.notifications){
			clearNotifications();
		}
		var notifications = JSON.parse(localStorage.notifications);
		var notificationOptions = localStorage['options'] ? JSON.parse(localStorage['options']) : {
			comments: true,
			replies: true,
			proofs: true
		};		
		if(notificationOptions.comments){
			notifications.newComments += data.newComments;			
			data.newTotal += data.newComments;
		}
		if(notificationOptions.replies){		
			notifications.newReply += data.newReply;
			data.newTotal += data.newReply;
		}
		if(notificationOptions.proofs){
			notifications.newProofs += data.newProofs;
			data.newTotal += data.newProofs;
		}
		notifications.newTotal += data.newTotal;
		notifications.processingProofs += data.processingProofs;
		notifications.fileToProcess = {};
		for(var idx in data.fileToProcess){
			var file = data.fileToProcess[idx];
			notifications.fileToProcess[file.file] = file;
		}
		data.messages.forEach(function(e){
			if(notificationOptions[e.type]){
				chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
				    chrome.tabs.sendMessage(tabs[0].id, {
				    	type: "displayNotification",
				    	data: {
				    		type: "success",
				    		content: e.message
				    	}
				    }, function() {});
				});
			}
		});
		localStorage.notifications = JSON.stringify(notifications);
		updateNotifications();
	});
};

chrome.runtime.onSuspend.addListener(function(){
	console.log("SUSPEND");
});

chrome.runtime.onStartup.addListener(function(){
	console.log("STARTUP");
	startupRoutine();
});

chrome.runtime.onInstalled.addListener(function(){
	console.log("INSTALLED");
	startupRoutine();
	chrome.contextMenus.removeAll(function(){
		if(localStorage.loggedIn === "true"){
	    	chrome.contextMenus.create({"title": "Convert to Proof", "contexts":["image"], "id": "addProofFromImage"});
		}				
	});
});

chrome.alarms.onAlarm.addListener(function(alarm){
	console.log("ALARM!", alarm);
	fetchNotifications();
});

chrome.runtime.onMessage.addListener(function(msg){
	switch(msg.type){
		case "updateNotifications":
			fetchNotifications();
			break;
		case "clearNotifications":
			clearNotifications();
			break;

	}
});

chrome.contextMenus.onClicked.addListener(function(info){
	console.log(info);
	switch(info.menuItemId){
		case "addProofFromImage":
			chrome.notifications.clear("addProofFromImage", function() {
				$.get(server+"?file="+encodeURIComponent(info.srcUrl+(info.srcUrl.match(/\?/) ? "&" : "?")+"r="+Date.now()), function(){
					chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
					    chrome.tabs.sendMessage(tabs[0].id, {
					    	type: "displayNotification",
					    	data: {
					    		type: "busy",
					    		content: "<b>Please wait...</b><br>Your proof is being processed.",
					    		timeout: 3000
					    	}
					    }, function() {});
					});
					setTimeout(fetchNotifications, 1000);
				});
			});
			break;
	}
});