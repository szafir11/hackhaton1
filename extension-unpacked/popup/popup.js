angular.module("popup", ["ui.router"])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
      url: "/login",
      views: {
        "page": {
        	templateUrl: "partials/login.html"
        },
      }
    })
    .state('index', {
      url: "/index",
      views: {
        "page": {
        	templateUrl: "partials/index.html"
        },
      }
    })
    .state('options', {
      url: "/options",
      views: {
        "page": {
        	templateUrl: "partials/options.html"
        },
      }
    });   
    $urlRouterProvider.otherwise('/login');
})

.controller("MainCtrl", ["$scope", "$state", function($scope, $state){
	$scope.notifications = {
		newTotal: 0,
		newComments: 0,
		newProofs: 0,
		newReply: 0,
	};
	chrome.runtime.onMessage.addListener(function(msg){
		console.log(msg);
		$scope.$apply(function(){
			switch(msg.type){
				case "notificationsUpdated":
					$scope.notifications = msg.data;
					break;
			}
		});
	});
	chrome.runtime.sendMessage(null, {
		type: "updateNotifications",
	});

	$scope.openOptions = function(){
		$state.go("options");
	};

	$scope.logOut = function(){
		localStorage.loggedIn = false;
		chrome.contextMenus.removeAll(function(){
			if(localStorage.loggedIn === "true"){
		    	chrome.contextMenus.create({"title": "Convert to Proof", "contexts":["image"], "id": "addProofFromImage"});
    		}				
		});		
		$state.go("login");
		chrome.browserAction.setBadgeText({text: ''});
	};

	$scope.clearNotifications = function(){
		chrome.runtime.sendMessage(null, {
			type: "clearNotifications",
		});
	};

	$scope.busy = false;
	$scope.$on("busy", function(them, isBusy){
		$scope.busy = isBusy;
	});

}])

.controller("LoginFormCtrl", ["$scope", "$state", "$timeout", "$rootScope", function($scope, $state, $timeout, $rootScope){
	$scope.email = "";
	$scope.password = "";
	$scope.submit = function(){
		$rootScope.$broadcast("busy", true);
		$timeout(function(){
			$rootScope.$broadcast("busy", false);
			localStorage.loggedIn = true;
			chrome.contextMenus.removeAll(function(){
				if(localStorage.loggedIn === "true"){
			    	chrome.contextMenus.create({"title": "Convert to Proof", "contexts":["image"], "id": "addProofFromImage"});
	    		}				
			});
			$state.go("index");
			chrome.runtime.sendMessage(null, {
				type: "updateNotifications",
			});
		}, 2000);
	};
	if(localStorage.loggedIn === "true"){
		$state.go("index");
	}
}])

.controller("OptionsCtrl", ["$scope", "$state", "$timeout", "$rootScope", function($scope, $state, $timeout, $rootScope){
	$scope.backToIndex = function(){
		$state.go("index");
	};
	$scope.options = localStorage['options'] ? JSON.parse(localStorage['options']) : {
		comments: true,
		replies: true,
		proofs: true
	};
	
	$scope.$watch('options', function() {
		localStorage['options'] = JSON.stringify($scope.options);
	}, true);
}])
;