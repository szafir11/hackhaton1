(function(){
	// isolated
	angular.module("phqContentScript", [])

	.directive("phqOverlay", ["$timeout", '$sce', function($timeout, $sce){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: $sce.trustAsResourceUrl(chrome.extension.getURL("contentScript/partials/overlay.html")),
			controller: function($scope){
				$scope.unslided=false;
				$scope.minified=false;
				$scope.progresses = localStorage.progresses ? JSON.parse(localStorage.progresses) : {};
				$scope.removeProgressInner = function(progress){
					delete $scope.progresses[progress.url];
					if(Object.keys($scope.progresses).length === 0){
						$scope.minified = false;
						$scope.unslided = false;
					}
					chrome.runtime.sendMessage(null, {
						type: "updateNotifications",
					});
					localStorage.progresses = JSON.stringify($scope.progresses);
				};
				var fakeDelay = function(progress){
					progress.percent = 100*(Date.now()/1000 - progress.start) / (progress.finish - progress.start);
					progress.msg = "";
					if(progress.canceled){
						progress.msg="Canceled";
						progress.percent = 100;
						$timeout(function(){
							$scope.removeProgress(progress);
						}, 5000);
					}else if(progress.percent>100){
						progress.percent = 100;
						progress.complete = true;
						progress.msg = "Complete!";
						$timeout(function(){
							$scope.removeProgress(progress);
						}, 5000);
					}
					// }else if(Math.random()>0.995){
					// 	progress.msg = "Some error!";
					// 	progress.percent = 100;
					// 	progress.error = true;
					// 	$timeout(function(){
					// 		$scope.removeProgress(progress);
					// 	}, 5000);
					// }
					else{
						$timeout(function(){
							fakeDelay(progress);
						}, Math.random()*500);
					}
					localStorage.progresses = JSON.stringify($scope.progresses);
				}

				$scope.toggle = function(){
					$scope.minified = !$scope.minified;
				}

				$scope.addProgress = function(newFile){
					var progress = {
						percent: 0,
						complete: false,
						error: null,
						url: newFile.file,
						finish: newFile.processTime,
						start: Date.now()/1000.0
					};
					if(!$scope.progresses[newFile.file]){
						$scope.progresses[newFile.file] = progress;
						fakeDelay(progress);
						$scope.unslided=true;
					}
					localStorage.progresses = JSON.stringify($scope.progresses);
				};

				$scope.cancelAll = function(){
					for(var name in $scope.progresses){
						var progress = $scope.progresses[name];
						if(!progress.error && !progress.complete && !progress.canceled ){
							progress.canceled = true;
						}
					};
					localStorage.progresses = JSON.stringify($scope.progresses);
				};

				chrome.extension.onMessage.addListener(function(msg) {
					$scope.$apply(function(){
						switch(msg.type){
							case "notificationsUpdated":
								for(var fileName in msg.data.fileToProcess){
									var file = msg.data.fileToProcess[fileName];
									$scope.addProgress(file)
								}
								break;
						}
					});
				});
				chrome.runtime.sendMessage(null, {
					type: "updateNotifications",
				});

				if($scope.progresses){
					var anyRunning = false;
					for(var idx in $scope.progresses){
						var progress = $scope.progresses[idx];
						if(!progress.error && !progress.complete && !progress.canceled ){
							anyRunning = true;
						}
						fakeDelay(progress);
					}
					$scope.unslided=anyRunning;
				}
			},
			link: function(scope, el){
				el.click(function(e){
					if(!$(e.target).is('.dontHide')){
						scope.$apply(scope.toggle);
					}
				});
				el.find('#phqIcon').attr('src', chrome.extension.getURL("res/icon-square.jpg"));
				scope.removeProgress = function(progress){
					el.find('[url="'+progress.url+'"]').fadeOut(function(){
						scope.$apply(function(){
							scope.removeProgressInner(progress);
						});
					});
				};
			},
		};
	}])

	.directive("phqNotification", ["$timeout", '$sce', function($timeout, $sce){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: $sce.trustAsResourceUrl(chrome.extension.getURL("contentScript/partials/notification.html")),
			controller: function($scope){
				$scope.ids = 0;
				$scope.notifications = {};

				$scope.removeNotification = function(notificationId){
					$scope.notifications[notificationId].fade = true;
					$timeout(function(){
						delete $scope.notifications[notificationId];
					}, 1000);
				};

				$scope.addNotification = function(type, content, timeout){
					var notification = {
						type: type,
						html: $sce.trustAsHtml(content),
						fade: false,
						id: $scope.ids++
					};
					$scope.notifications[notification.id] = notification;
					$timeout(function(){
						$scope.removeNotification(notification.id);
					}, timeout||8000);
					$timeout(function(){
						$scope.rebindDom();
					}, 0);
				};
				chrome.extension.onMessage.addListener(function(msg) {
					console.log(msg)
					$scope.$apply(function(){
						switch(msg.type){
							case "displayNotification":
								$scope.addNotification(msg.data.type, msg.data.content, msg.data.timeout);
								break;
						}
					});
				});
			},
			link: function(scope, el){
				scope.rebindDom = function(){
					el.find('.icon-success').attr('src', chrome.extension.getURL("res/icon-success.png"));
					el.find('.icon-fail').attr('src', chrome.extension.getURL("res/icon-fail.png"));
					el.find('.icon-busy').attr('src', chrome.extension.getURL("res/icon-busy.png"));
					el.find('.notification').bind('mousedown touchstart', function(e){
						$(this).data('lastX', e.pageX);
						$(this).data('lastTS', Date.now());
					});
					el.find('.notification').bind('mouseup touchend', function(e){
						var velocity = (e.pageX - $(this).data('lastX'))/(Date.now()-$(this).data('lastTS'));
						if(velocity>1){
							var nid = $(this).attr('nid');
							scope.$apply(function(){
								scope.removeNotification(nid);
							});
						}
					});
				};
			},
		};
	}])

	;

	var el = $('<phq-overlay></phq-overlay>');
	$("body").append(el);
	angular.bootstrap(el[0], ['phqContentScript']);
	var el2 = $('<phq-notification></phq-notification>');
	$("body").append(el2);
	angular.bootstrap(el2[0], ['phqContentScript']);
})();