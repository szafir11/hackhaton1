console.log("RUN");

chrome.runtime.onSuspend.addListener(function(){
	console.log("SUSPEND");
});

chrome.runtime.onStartup.addListener(function(){
	console.log("STARTUP");
});

chrome.runtime.onInstalled.addListener(function(){
	console.log("INSTALLED");
});

chrome.alarms.onAlarm.addListener(function(alarm){
	console.log("ALARM!", alarm);
	var n = localStorage.n;
	$("#n").text(n);
});

var n = localStorage.n;
$("#n").text(n);
