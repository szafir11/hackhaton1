function c(el) {
	console.log(el);
}
var uploader ={};

$(document).ready(function() {
	$(".form-signin").submit(function(event) {
		event.preventDefault();
		var data = $(this).serializeArray();
		
		$.each(data, function(i, item) {
			localStorage[item.name] = item.value;
		})
		location.href = 'index.html';
	})
	$("#submit-proof").click(function() {
		location.href = 'proofSubmited.html';
	})
	
	uploader = {
		counter: 0,
		current: 1,
		cont : $("#proof-list ul"),
		submitButton: $("#submit-proof"),
		currentProgress: 0,
		upload: function() {
			this.submitButton.attr('disabled', true);
			this.element = this.cont.find('[data-id='+this.current+'] .progress-bar');
			if(this.element.length) {
				this.updateProgress();
			}
			else {
				this.submitButton.attr('disabled', false);
			}
		},
		updateProgress: function() {
			if(this.currentProgress < 100) {
				var self = this;
				var step = parseInt(Math.random() * 50 );
				this.currentProgress += step;
				this.element.width(this.currentProgress + '%')
				setTimeout(function() {
					self.updateProgress();
				}, 1000)
			}
			else {
				this.currentProgress = 0;
				this.current++;
				this.upload();
			}
		}
	}
	$("#inputFile").change(function(e) {
		var files = this.files,
			cont = $("#proof-list ul");
		
		$.each(files, function(i, item) {
			uploader.counter++;
			var html = '<li class="list-group-item" data-id="'+uploader.counter+'">' + 
				'<p>'+item.name+'</p>'+
				'<div class="progress">'+
				'<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">'+
				'</div>'+
				'</div>'+
				'</li>';
			cont.append(html);
		})
		
		uploader.upload();
	})
	
	
	
	
	
	$("#add-recipient").click(function() {
		var html = '<li class="list-group-item row">'+
					'	<div class="col-xs-12 col-sm-6 col-md-8">'+
						$("#recipients-input").val()+
					'	</div>'+
					'	<div class="col-xs-6 col-md-4">'+
					'		<select>'+
					'			<option value="1">Read only</option>'+
					'			<option value="3">Reviewer</option>'+
					'			<option value="4">Approver</option>'+
					'			<option value="5">Reviewer &amp; Approver</option>'+
					'			<option value="6">Author</option>'+
					'			<option value="7">Moderator</option>'+
					'		</select>'+
					'	</div>'+
					'</li>';
		$("#recipient-list").append(html);
		$("#recipients-input").val('');
	})
	
	$("#recipients-input").keyup(function(e) {
		if(e.keyCode == 13) {
			$("#add-recipient").click();
		}
	})
})